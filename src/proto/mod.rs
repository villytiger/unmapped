use std::io::{Read, Write};

use num_enum::TryFromPrimitive;

pub const SOCKET_PATH: &'static str = "/var/run/unmapped/unmapped.sock";
pub const VERSION: u32 = 1;

#[derive(Debug, TryFromPrimitive)]
#[repr(u32)]
pub enum Command {
    MapUid,
    MapGid
}

quick_error! {
    #[derive(Debug)]
    pub enum Error {
        Write(err: std::io::Error) {
            cause(err)
            display("Error writing to socket: {}", err)
        }

        Read(err: std::io::Error) {
            cause(err)
            display("Error reading from socket: {}", err)
        }
    }
}

pub trait Serializable {
    type Serialized = [u8];

    fn to_ne_bytes(self: Self) -> Self::Serialized;

    fn from_ne_bytes(bytes: Self::Serialized) -> Self;
}

macro_rules! serializable {
    ($size:expr, $type:ty) => {
        impl Serializable for $type {
            type Serialized = [u8; $size];

            fn to_ne_bytes(self: $type) -> [u8; $size] {
                self.to_ne_bytes()
            }

            fn from_ne_bytes(bytes: [u8; $size]) -> $type {
                <$type>::from_ne_bytes(bytes)
            }
        }
    }
}

serializable!(4, i32);
serializable!(4, u32);
serializable!(8, i64);
serializable!(8, u64);
/*
impl Serializable for i32 {
    type Serialized = [u8; 4];

    fn to_ne_bytes(self) -> [u8; 4] {
        self.to_ne_bytes()
    }

    fn from_ne_bytes(bytes: [u8; 4]) -> Self {
        i32::from_ne_bytes(bytes)
    }
}
*/

pub fn write<W: Write, T: Serializable>(writer: &mut W, value: T) -> Result<(), Error> {
    let bytes = value.to_ne_bytes();
    writer.write_all(&bytes).map_err(|e| Error::Write(e))
}

pub fn read<R: Read>(reader: &mut R) -> Result<u64, Error> {
    let mut buf = [0u8; std::mem::size_of::<u64>()];
    reader.read_exact(&mut buf).map_err(|e| Error::Read(e))?;
    Ok(u64::from_ne_bytes(buf))
}

