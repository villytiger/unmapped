use std::io;
use std::io::BufRead;
use std::fs;
use std::os::unix::fs::MetadataExt;
use std::os::unix::io::*;

use libc;

quick_error! {
    #[derive(Debug)]
    pub enum Error {
        Io(err: io::Error) {
            cause(err)
        }

        NotFound {
        }

        PermissionDenied {
        }

        ParseInt(desc: &'static str, err: std::num::ParseIntError) {
            cause(err)
            display("{}: {}", desc, err)
        }
    }
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Self {
        match err.kind() {
            io::ErrorKind::NotFound => Error::NotFound,
            io::ErrorKind::PermissionDenied => Error::PermissionDenied,
            _ => Error::Io(err)
        }
    }
}

const NS_GET_PARENT: libc::c_ulong = 46850;

pub struct PidNs {
    file: Option<fs::File>
}

impl Iterator for PidNs {
    type Item = Result<libc::ino_t, io::Error>;

    fn next(&mut self) -> Option<Self::Item> {
        let file = match self.file {
            Some(ref v) => v,
            None => return None
        };

        let ns = match file.metadata() {
            Ok(v) => v.ino(),
            Err(e) => { return Some(Err(e)) }
        };

        let parent = unsafe { libc::ioctl(file.as_raw_fd(), NS_GET_PARENT) };
        if parent == -1 {
            let error = io::Error::last_os_error();
            if error.raw_os_error().unwrap() == libc::EPERM {
                self.file = None;
            } else {
                return Some(Err(error))
            }
        } else {
            self.file = unsafe { Some(fs::File::from_raw_fd(parent)) };
        }

        Some(Ok(ns))
    }
}

pub fn iter_pid_ns(real_pid: libc::pid_t) -> Result<PidNs, io::Error> {
    let file = match fs::File::open(format!("/proc/{}/ns/pid", real_pid)) {
        Ok(v) => Some(v),
        Err(e) if e.kind() == io::ErrorKind::NotFound => None,
        Err(e) => return Err(e)
    };

    Ok(PidNs{file: file})
}

pub fn get_pid_ns(real_pid: libc::pid_t) -> Result<libc::ino_t, io::Error> {
    Ok(fs::metadata(format!("/proc/{}/ns/pid", real_pid))?.ino())
}

pub fn get_virt_pid(ns: libc::ino_t, real_pid: libc::pid_t) -> Result<libc::pid_t, Error> {
    let mut it = iter_pid_ns(real_pid)?;
    let mut level = None;
    loop {
        match (level, it.next()) {
            (_,       Some(Err(e))    )                 => return Err(e.into()),
            (_,       Some(Ok(pid_ns))) if pid_ns == ns => level = Some(0),
            (Some(l), Some(Ok(_))     )                 => level = Some(l + 1),
            (None,    Some(Ok(_))     )                 => (),
            (None,    None            )                 => return Err(Error::NotFound),
            (Some(_), None            )                 => break
        }
    }

    let file = fs::File::open(format!("/proc/{}/status", real_pid))?;
    let mut nspid_info = None;
    for line in io::BufReader::new(file).lines() {
        let line = line?;
        if line.starts_with("NSpid:") {
            nspid_info = Some(line[6..].trim().to_string());
            break;
        }
    }

    let virt_pid = nspid_info.ok_or(Error::NotFound)?
        .split_whitespace().nth(level.unwrap()).ok_or(Error::NotFound)?
        .parse::<libc::pid_t>().map_err(|e| Error::ParseInt("Error parsing NSpid", e))?;
    Ok(virt_pid)
}

pub fn get_real_pid(ns: libc::ino_t, virt_pid: libc::pid_t) -> Result<libc::pid_t, Error> {
    for entry in fs::read_dir("/proc")? {
        let file_name = match entry?.file_name().into_string() {
            Ok(v) => v,
            Err(_) => continue
        };

        let real_pid = match file_name.parse::<libc::pid_t>() {
            Ok(v) => v,
            Err(_) => continue
        };

        match get_virt_pid(ns, real_pid) {
            Ok(pid) if pid == virt_pid => return Ok(real_pid),
            Ok(_) | Err(Error::NotFound) | Err(Error::PermissionDenied) => continue,
            Err(e) => return Err(e)
        }
    }

    Err(Error::NotFound)
}
