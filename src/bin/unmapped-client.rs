use std::env;
use std::io::Read;
use std::io::Write;
use std::os::unix::net::UnixStream;
use std::path::Path;
use std::process::exit;

#[macro_use] extern crate quick_error;

quick_error! {
    #[derive(Debug)]
    pub enum AppError {
        String(desc: String) {
            display("{}", desc)
            from()
        }

        StaticStr(desc: &'static str) {
            display("{}", desc)
            from()
        }

        Remote(code: i32, desc: String) {
            display("{}", desc)
        }
    }
}

enum Command {
    MapUid,
    MapGid
}

const SOCKET_PATH: &'static str = "/var/run/unmapped/unmapped.sock";
const VERSION: i32 = 1;

fn write<W: Write>(writer: &mut W, value: i32) -> Result<(), AppError> {
    writer.write_all(&(value as i32).to_ne_bytes())
        .map_err(|e| format!("Error writing to the socket: {}", e))?;
    Ok(())
}

fn read<R: Read>(reader: &mut R) -> Result<i32, AppError> {
    let mut buf = [0u8; std::mem::size_of::<i32>()];
    reader.read_exact(&mut buf)
        .map_err(|e| format!("Error reading from the socket: {}", e))?;
    Ok(i32::from_ne_bytes(buf))
}

fn run(command: Command, args: &[i32]) -> Result<(), AppError> {
    let mut stream = UnixStream::connect(SOCKET_PATH)
        .map_err(|e| format!("Error opening socket \"{}\": {}", SOCKET_PATH, e))?;

    write(&mut stream, VERSION)?;
    if read(&mut stream)? != VERSION {
        return Err("Unsupported protocol version from the server".into())
    }

    write(&mut stream, command as i32)?;
    for arg in args { write(&mut stream, *arg)?; }

    let code = read(&mut stream)?;
    if code != 0 {
        let size = read(&mut stream)?;
        if size > 1024 { return Err("Too long string from the server".into()) }

        let mut buf = Vec::new();
        stream.take(size as u64).read_to_end(&mut buf)
            .map_err(|e| format!("Error reading from the socket: {}", e))?;

        let desc = String::from_utf8(buf)
            .map_err(|e| format!("Error parsing string from the server: {}", e))?;

        return Err(AppError::Remote(code, desc));
    }

    Ok(())
}

fn help(file_name: &str) -> ! {
    eprintln!("Usage: {} <pid> <uid> <loweruid> <count> [ <uid> <loweruid> <count> ] ...", file_name);
    exit(1);
}

fn main() {
    let command: Command;
    let args: Vec<String> = env::args().collect();

    let file_name = Path::new(&args[0]).file_name().map_or("", |osstr| osstr.to_str().unwrap_or(""));
    match file_name {
        "newuidmap" | "mapuid" => command = Command::MapUid,
        "newgidmap" | "mapgid" => command = Command::MapGid,
        _ => {
            eprintln!("Wrong file name, must be one of newuidmap, newgidmap, mapuid, mapgid");
            exit(1);
        }
    }

    if args.len() < 5 || (args.len() - 2) % 3 != 0 {
        help(file_name);
    }

    let int_args: Result<Vec<i32>, _> = args[1..].into_iter().map(|a| a.parse::<i32>()).collect();
    if int_args.is_err() {
        eprintln!("Error parsing arguments: {}", int_args.unwrap_err());
        help(file_name);
    }

    match run(command, &int_args.unwrap()[1..]) {
        Ok(_) => (),
        Err(AppError::Remote(code, desc)) => {
            eprintln!("{}", desc);
            exit(code);
        },
        Err(e) => {
            eprintln!("{}", e);
            exit(1);
        },
    }
}
