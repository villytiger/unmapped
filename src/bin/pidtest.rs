use unmapped::pid;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Vec<String> = std::env::args().collect();

    let known_real_pid = args[1].parse::<libc::pid_t>()?;
    let ns = pid::get_pid_ns(known_real_pid)?;

    let virt_pid = args[2].parse::<libc::pid_t>()?;
    let real_pid = pid::get_real_pid(ns, virt_pid)?;

    println!("{}", real_pid);
    Ok(())
}
