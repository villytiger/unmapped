use std::convert::TryFrom;
use std::fs;
use std::io;
use std::io::prelude::*;
use std::os::unix::io::*;
use std::os::unix::net::{UnixListener, UnixStream};
use std::os::unix::process::CommandExt;

use libc::{gid_t, uid_t, pid_t};

use nix::poll::{PollFlags, PollFd, poll};
use nix::sys::signal::{SigSet, Signal, SigmaskHow};
use nix::sys::signalfd::{SignalFd, SfdFlags};
use nix::sys::socket::{UnixCredentials, sockopt, getsockopt};
use nix::unistd::{Gid, Uid, setresgid, setresuid};

use unmapped::pid;
use unmapped::proto;

#[macro_use] extern crate quick_error;

//const SOCKET_PATH: &'static str = "/var/run/unmapped/unmapped.sock";
const SOCKET_PATH: &'static str = "unmapped.sock";

quick_error! {
    #[derive(Debug)]
    pub enum AppError {
        Io(err: io::Error) {
            cause(err)
            from()
            display("{}", err)
        }

        Nix(err: nix::Error) {
            cause(err)
            from()
        }

        Proto(err: proto::Error) {
            cause(err)
            from()
            display("{}", err)
        }

        Pid(err: pid::Error) {
            cause(err)
            from()
            display("{}", err)
        }

        Str(desc: &'static str) {
            display("{}", desc)
            from()
        }
    }
}

type Result<T> = std::result::Result<T, AppError>;

struct Listener {
    listener: UnixListener,
    signalfd: SignalFd,
    sigset: SigSet
}

impl Drop for Listener {
    fn drop(&mut self) {
        self.sigset.thread_set_mask().unwrap();
    }
}

impl Listener {
    fn new<P: AsRef<std::path::Path>>(path: P) -> Result<Listener> {
        match std::fs::remove_file(SOCKET_PATH) {
            Ok(_) => (),
            Err(e) if e.kind() == std::io::ErrorKind::NotFound => (),
            Err(e) => return Err(e.into())
        }

        let listener = UnixListener::bind(path)?;
        listener.set_nonblocking(true)?;

        let mut sigset = SigSet::empty();
        sigset.add(Signal::SIGINT);
        sigset.add(Signal::SIGTERM);

        let signalfd = SignalFd::with_flags(&sigset, SfdFlags::SFD_NONBLOCK)?;
        let sigset = sigset.thread_swap_mask(SigmaskHow::SIG_BLOCK)?;

        Ok(Listener{listener: listener, signalfd: signalfd, sigset: sigset})
    }

    fn accept(&mut self) -> Result<Option<UnixStream>> {
        let mut fds = [
            PollFd::new(self.signalfd.as_raw_fd(), PollFlags::POLLIN),
            PollFd::new(self.listener.as_raw_fd(), PollFlags::POLLIN)
        ];

        if let Err(e) = poll(&mut fds, -1) {
            return Err(e.into())
        }

        if fds[0].revents().unwrap().contains(PollFlags::POLLIN) {
            self.signalfd.read_signal()?;
            return Ok(None)
        }

        self.listener.accept().map(|(s, _)| Some(s)).map_err(|e| e.into())
    }
}

fn run_idmap(program: proto::Command, creds: UnixCredentials, int_args: &[u64]) -> Result<std::process::Output> {
    let program = match program {
        proto::Command::MapUid => "newuidmap",
        proto::Command::MapGid => "newgidmap"
    };

    let mut args = vec![creds.pid().to_string()];
    args.extend(int_args.into_iter().map(|a| a.to_string()));

    let uid = Uid::from_raw(creds.uid());
    let max_uid = Uid::from_raw(uid_t::max_value());
    let gid = Gid::from_raw(creds.gid());
    let max_gid = Gid::from_raw(gid_t::max_value());

    unsafe {
        std::process::Command::new(program).args(args).pre_exec(move || {
            setresgid(gid, max_gid, max_gid).and_then(|_| {
                setresuid(uid, max_uid, max_uid)
            }).map_err(|e| {
                match e {
                    nix::Error::Sys(_) => io::Error::last_os_error(),
                    _ => io::Error::new(io::ErrorKind::Other, "setresuid or setresgid has failed")
                }
            })
        }).output().map_err(|e| e.into())
    }
}

fn serve(mut stream: UnixStream) -> Result<()> {
    let creds = getsockopt(stream.as_raw_fd(), sockopt::PeerCredentials)?;
    let pid_ns = pid::get_pid_ns(creds.pid())?;

    let version = proto::read(&mut stream)?;
    if version != proto::VERSION {
        return Err(AppError::Str("Wrong protocol version"))
    }

    let command = proto::Command::try_from(proto::read(&mut stream)?)
        .map_err(|_| AppError::Str("Wrong command"))?;

    let virt_pid = proto::read(&mut stream)?;
    let real_pid = pid::get_real_pid(pid_ns, virt_pid as pid_t)?;

    let args_size = proto::read(&mut stream)?;
    if args_size < 0 || args_size > 1024 {
        return Err(AppError::Str("Wrong number of arguments"))
    }

    let mut args = Vec::with_capacity(args_size as usize);
    for _ in 0..args_size {
        args.push(proto::read(&mut stream)?);
    }

    let output = run_idmap(command, creds, &args)?;
    proto::write(&mut stream, output.status.code().unwrap_or(1) as u64)?;
    proto::write(&mut stream, output.stdout.len() as u64)?;
    stream.write_all(&output.stdout)?;

    Ok(())
}

fn run() -> Result<()> {
    let mut listener = Listener::new(SOCKET_PATH)?;
    while let Some(stream) = listener.accept()? {
        serve(stream)?;
    }

    std::fs::remove_file(SOCKET_PATH)?;

    Ok(())
}

fn main() {
    if let Err(e) = run() {
        eprintln!("{}", e);
        std::process::exit(1);
    }
}
